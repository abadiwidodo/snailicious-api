﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Snailicious.Api.Models
{
    public class Recipe
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Tag { get; set; }
        public string UserId { get; set; }
        public string Image { get; set; }

        public decimal SalePrice { get; set; }
        public string Location { get; set; }

        //Table relationship
        public ApplicationUser User { get; set; }
        public List<Like> Likes { get; set; }
        public List<Comment> Comments { get; set; }
    }
}