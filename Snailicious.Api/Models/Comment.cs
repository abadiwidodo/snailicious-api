﻿using System;

namespace Snailicious.Api.Models
{
    public class Comment
    {
        public int Id { get; set; }
        public int RecipeId { get; set; }
        public string Message { get; set; }
        public string UserId { get; set; }
        public DateTime DateCreated { get; set; }

        //Table relationship
        public Recipe Recipe { get; set; }
        public ApplicationUser User { get; set; }
    }
}