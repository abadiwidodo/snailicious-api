﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Snailicious.Api.Models
{
    public class Like
    {
        public int Id { get; set; }
        public int RecipeId { get; set; }
        public string UserId { get; set; }
        public DateTime DateCreated { get; set; }

        public Recipe Recipe { get; set; }
        public ApplicationUser User { get; set; }
    }
}