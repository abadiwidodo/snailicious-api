﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Snailicious.Api.Client.Helper;
using Snailicious.Api.Client.Model.Account;
using Snailicious.Api.Client.Model.Account.Request;

namespace Snailicious.Api.Client
{
    public class AccountApiClient : WebAPIHelper
    {
        //private ILog _log = LogManager.GetLogger("AccountAPIClient");

        public AccountApiClient(string token = "")
            : base(ConfigurationManager.AppSettings["ApiBaseUrl"] + "account/", token)
        {
            //need authorization, so pass the access_token
        }

        //public List<ExternalLogin> GetExternalLogins()
        //{
        //    ///api/Account/ExternalLogins?returnUrl=%2F&generateState=true
        //    return Get<List<ExternalLogin>>("ExternalLogins?returnUrl=%2F&generateState=true");
        //}

        public User GetUser(string token)
        {
            return Get<User>("userinfo", token);
        }

        public User GetUser()
        {
            return Get<User>("userinfo");
        }

        public RegisterUser RegisterUser(RegisterUser user)
        {
            string json = Post<RegisterUser>("register", user);
            return user;
        }

        public bool Logout()
        {
            HttpClient client = new HttpClient();
            var pairs = new List<KeyValuePair<string, string>>
            {
            };

            var content = new FormUrlEncodedContent(pairs);
            HttpResponseMessage response = client.PostAsync(ConfigurationManager.AppSettings["ApiBaseUrl"] + "account/" + "logout", content).Result;
            var result = response.Content.ReadAsStringAsync().Result;

            return true;
        }

        //public RegisterExternalUser RegisterExternal(RegisterExternalUser user)
        //{
        //    string json = POST<RegisterExternalUser>("registerexternal", user);
        //    return user;
        //}

        public AddExternalLoginBinding AddExternalLogin(AddExternalLoginBinding addExternalLoginBindingModel)
        {
            string json = Post<AddExternalLoginBinding>("AddExternalLogin", addExternalLoginBindingModel);
            return addExternalLoginBindingModel;
        }

        public User SaveUser(User user)
        {
            string json = Post<User>("updatedetail", user);
            return user;
        }

        public User SaveUserSetting(User user)
        {
            string json = Post<User>("updatesetting", user);
            return user;
        }

        public User SaveAvatar(User user)
        {
            string json = Post<User>("updateavatar", user);
            return user;
        }

        /// <summary>
        ///     Posts the specified URL.
        /// </summary>
        /// <typeparam name="TJson">The type of the json.</typeparam>
        /// <param name="requestedScope">The oAuth scope required to run this request</param>
        /// <param name="url">The URL.</param>
        /// <param name="data">The data.</param>
        /// <returns></returns>
        /// <exception cref="System.Net.Http.HttpRequestException"></exception>
        private async Task<string> PostAsync<TJson>(string url, TJson data)
        {
            var result = string.Empty;

            using (var client = new HttpClient())
            {
                client.Timeout = TimeSpan.FromSeconds(10);

                var content = SerializeContent(data);
                using (var response = await client.PostAsync(new Uri(url), content).ConfigureAwait(false))
                {
                    if (response.StatusCode != HttpStatusCode.Created)
                    {
                        throw new HttpRequestException(response.ReasonPhrase);
                    }

                    if (response.Headers != null && response.Headers.Location != null)
                    {
                        result = response.Headers.Location.ToString();
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Serializes the specified data.
        /// </summary>
        /// <typeparam name="TJson">The type of the json.</typeparam>
        /// <param name="data">The data.</param>
        /// <returns></returns>
        private static string Serialize<TJson>(TJson data)
        {
            string json;

            using (var stringWriter = new StringWriter())
            {
                using (var jsonTextWriter = new JsonTextWriter(stringWriter))
                {
                    Serializer.Serialize(jsonTextWriter, data);
                    json = stringWriter.ToString();
                }
            }
            return json;
        }

        /// <summary>
        /// Serializes the content.
        /// </summary>
        /// <typeparam name="TJson">The type of the json.</typeparam>
        /// <param name="data">The data.</param>
        /// <returns></returns>
        private StringContent SerializeContent<TJson>(TJson data)
        {
            var json = Serialize(data);
            return new StringContent(json, Encoding.UTF8, "application/json");
        }

        private static readonly JsonSerializer Serializer = new JsonSerializer
        {
            ContractResolver = new CamelCasePropertyNamesContractResolver(),
            NullValueHandling = NullValueHandling.Ignore,
            DefaultValueHandling = DefaultValueHandling.Ignore
        };
    }

    public class AddExternalLoginBinding
    {
        public string ExternalAccessToken { get; set; }
    }
}
