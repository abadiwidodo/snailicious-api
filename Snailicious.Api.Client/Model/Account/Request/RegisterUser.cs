﻿namespace Snailicious.Api.Client.Model.Account.Request
{
    public class RegisterUser
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }
}
