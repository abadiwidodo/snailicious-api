﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

//http://www.developer.com/net/asp/consuming-an-asp.net-web-api-using-httpclient.html
namespace Snailicious.Api.Client.Helper
{
    public abstract class WebAPIHelper
    {
        private string url = "";
        private string access_token = "";
        private HttpClient client = new HttpClient();

        private static readonly JsonSerializer Serializer = new JsonSerializer
        {
            ContractResolver = new CamelCasePropertyNamesContractResolver(),
            NullValueHandling = NullValueHandling.Ignore,
            DefaultValueHandling = DefaultValueHandling.Ignore
        };

        private static readonly JsonSerializerSettings SerializerSettings = new JsonSerializerSettings
        {
            ContractResolver = new CamelCasePropertyNamesContractResolver(),
            NullValueHandling = NullValueHandling.Ignore,
            DefaultValueHandling = DefaultValueHandling.Ignore
        };

        public WebAPIHelper(string url, string access_token = "")
        {
            this.url = url;
            this.access_token = access_token;
            if (!String.IsNullOrEmpty(this.access_token))
            {
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", this.access_token);
            }
        }

        public void SetAccessToken(string access_token)
        {
            this.access_token = access_token;
            if (!String.IsNullOrEmpty(this.access_token))
            {
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", this.access_token);
            }
        }

        #region GET
        public TJson GetJson<TJson>(string segmentUrl, IDictionary<string, string> headers = null) where TJson : class
        {
            using (var response = client.GetAsync(this.url + segmentUrl).Result)
            {
                if (response.IsSuccessStatusCode)
                {
                    using (var responseContent = response.Content.ReadAsStreamAsync().Result)
                    {
                        using (var reader = new StreamReader(responseContent, Encoding.UTF8))
                        {
                            var result = reader.ReadToEnd();
                            return JsonConvert.DeserializeObject<TJson>(result, SerializerSettings);
                        }
                    }
                }
            }
            return null;
        }

        public T Get<T>(string urlSegment) where T : class
        {
            using (HttpResponseMessage response = client.GetAsync(this.url + urlSegment).Result)
            {
                if (response.IsSuccessStatusCode)
                {
                    T obj = JsonConvert.DeserializeObject<T>(response.Content.ReadAsStringAsync().Result);
                    return obj;
                }
            }
            return null;
        }

        public bool GetBool(string urlSegment)
        {
            using (HttpResponseMessage response = client.GetAsync(this.url + urlSegment).Result)
            {
                if (response.IsSuccessStatusCode)
                {
                    bool obj = JsonConvert.DeserializeObject<bool>(response.Content.ReadAsStringAsync().Result);
                    return obj;
                }
            }
            return false;
        }


        /// <summary>
        ///     Get JSON data via fetching from given URL.
        /// </summary>
        /// <typeparam name="T">The type of the JSON object returned by the service</typeparam>
        /// <param name="scope">The scope.</param>
        /// <param name="url">The serivce URL</param>
        /// <returns>
        ///     A generic Task object of user specified type
        /// </returns>
        public async Task<T> GetAsync<T>(string url) where T : class
        {
            using (client)
            {
                //client.Timeout = ;
                using (var response = await client.GetAsync(new Uri(this.url + url)).ConfigureAwait(false))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        using (var stream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
                        {
                            return Deserialize<T>(stream);
                        }
                    }
                }
            }

            return null;
        }

        public T Get<T>(string urlSegment, string accessToken)
        {
            // Add a new Request Message
            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Get, url + urlSegment);
            // Add our custom user agent
            requestMessage.Headers.Add("Authorization", "Bearer " + accessToken);

            // Send the request to the server
            HttpResponseMessage response = client.SendAsync(requestMessage).Result;

            //HttpResponseMessage response = client.GetAsync(this.url + urlSegment).Result;
            T data = JsonConvert.DeserializeObject<T>(response.Content.ReadAsStringAsync().Result);
            return data;
        }
        #endregion

        #region POST
        /// <summary>
        ///     Posts the specified URL.
        /// </summary>
        /// <typeparam name="T">The type of the json.</typeparam>
        /// <param name="requestedScope">The oAuth scope required to run this request</param>
        /// <param name="url">The URL.</param>
        /// <param name="data">The data.</param>
        /// <returns></returns>
        /// <exception cref="System.Net.Http.HttpRequestException"></exception>
        public async Task<string> PostAsync<T>(string url, T data)
        {
            var result = string.Empty;

            using (var client = new HttpClient())
            {
                //client.Timeout = DefaultTimeout;

                var content = SerializeContent(data);
                using (var response = await client.PostAsync(new Uri(this.url + url), content).ConfigureAwait(false))
                {
                    if (response.StatusCode != HttpStatusCode.Created)
                    {
                        throw new HttpRequestException(response.ReasonPhrase);
                    }

                    if (response.Headers != null && response.Headers.Location != null)
                    {
                        result = response.Headers.Location.ToString();
                    }
                }
            }

            return result;
        }

        public string Post<T>(T obj)
        {
            HttpRequestMessage request = new HttpRequestMessage();
            
            var content = new ObjectContent<T>(obj, new JsonMediaTypeFormatter());

            HttpResponseMessage response = client.PostAsync(url, content).Result;
            T data = JsonConvert.DeserializeObject<T>(response.Content.ReadAsStringAsync().Result);
            return data != null ? data.ToString() : "";
        }

        public string Post<T>(string urlsegment, string accessToken, T obj)
        {
            HttpRequestMessage request = new HttpRequestMessage();
            request.RequestUri = new Uri(url + urlsegment);
            // Add our custom user agent
            request.Headers.Add("Authorization", "Bearer " + accessToken);
            var content = new ObjectContent<T>(obj, new JsonMediaTypeFormatter());

            request.Content = content;
            request.Method = HttpMethod.Post;

            // Send the request to the server
            HttpResponseMessage response = client.SendAsync(request).Result;

            var data = JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result);
            return data != null ? data.ToString() : "";
        }

        public string Post<T>(string urlsegment, T obj)
        {
            HttpRequestMessage request = new HttpRequestMessage();
            request.RequestUri = new Uri(url + urlsegment);

            var content = new ObjectContent<T>(obj, new JsonMediaTypeFormatter());
            request.Content = content;

            request.Method = HttpMethod.Post;

            // Send the request to the server
            HttpResponseMessage response = client.SendAsync(request).Result;

            var data = JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result);
            return data != null ? data.ToString() : "";
        }
        #endregion

        #region PUT
        public string Put<T>(string urlsegment, T obj)
        {
            HttpRequestMessage request = new HttpRequestMessage();
            var content = new ObjectContent<T>(obj, new JsonMediaTypeFormatter());
            HttpResponseMessage response = client.PutAsync(url + urlsegment, content).Result;
            if (response.IsSuccessStatusCode)
            {
                T data = JsonConvert.DeserializeObject<T>(response.Content.ReadAsStringAsync().Result);
                if (data == null)
                    return "OK";
                return data.ToString();
            }
            else
            {
                return "Error";
            }

        }
        #endregion

        #region DELETE
        public string Delete<T>(string urlsegment, T obj)
        {
            HttpRequestMessage request = new HttpRequestMessage();
            HttpResponseMessage response = client.DeleteAsync(url + urlsegment).Result;
            T data = JsonConvert.DeserializeObject<T>(response.Content.ReadAsStringAsync().Result);
            return data.ToString();
        }

        public void Delete<TId>(TId id)
        {
            using (var response = client.DeleteAsync(new Uri(url + "/" + Uri.EscapeDataString(id.ToString()))).Result)
            {
                if (response.StatusCode != HttpStatusCode.OK)
                    throw new HttpRequestException(response.ReasonPhrase);
            }
        }
        #endregion

        #region Helpers
        /// <summary>
        ///     Serializes the content.
        /// </summary>
        /// <typeparam name="T">The type of the json.</typeparam>
        /// <param name="data">The data.</param>
        /// <returns></returns>
        private StringContent SerializeContent<T>(T data)
        {
            string json;

            using (var stringWriter = new StringWriter())
            {
                using (var jsonTextWriter = new JsonTextWriter(stringWriter))
                {
                    Serializer.Serialize(jsonTextWriter, data);
                    json = stringWriter.ToString();
                }
            }

            return new StringContent(json, Encoding.UTF8, "application/json");
        }

        /// <summary>
        ///     Deserializes the specified stream.
        /// </summary>
        /// <typeparam name="TJson">The type of the json.</typeparam>
        /// <param name="stream">The stream.</param>
        /// <returns></returns>
        private static TJson Deserialize<TJson>(Stream stream) where TJson : class
        {
            using (var stringReader = new StreamReader(stream))
            {
                using (var jsonReader = new JsonTextReader(stringReader))
                {
                    return Serializer.Deserialize<TJson>(jsonReader);
                }
            }
        }
        #endregion
    }
}
